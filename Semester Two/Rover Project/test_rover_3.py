import time
try:
    import usocket as socket
except:
    import socket

from machine import Pin
import network
from machine import PWM, Pin
from time import sleep
from hcsr04 import HCSR04
import _thread
import globalVar
import autoloop
import sys

autoloop.Stop()

# Setup AP. Remember to change essid.
# station = network.WLAN(network.AP_IF)
# station.active(True)
# station.config(essid = 'team_one')
# station.config(authmode=3, password = '12345678')
# 
# while station.isconnected() == False:
#     pass
# 
# print('Connection successful')
# print(station.ifconfig())

def map(x, in_min, in_max, out_min, out_max):
    return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)

class GORILLACELL_SERVO:
    def __init__(self, signal_pin):
        self.pwm = PWM(Pin(signal_pin), freq=50, duty=75)

    def rotate(self, angle):
        self.pwm.duty(map(angle, 0, 180, 23, 124))

servo1 = GORILLACELL_SERVO(signal_pin=23)
servo2 = GORILLACELL_SERVO(signal_pin=22)
servo3 = GORILLACELL_SERVO(signal_pin=19)
servo4 = GORILLACELL_SERVO(signal_pin=21)

servo3.rotate(90)

def Reset():
    servo1 = GORILLACELL_SERVO(signal_pin=23)
    servo2 = GORILLACELL_SERVO(signal_pin=22)
    servo3 = GORILLACELL_SERVO(signal_pin=19)
    servo4 = GORILLACELL_SERVO(signal_pin=21)
    

def web_page(request):
    html = '''<html><head> <title>Rover Web Server</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="icon" href="data:,"> <style>
 html{font-family: Helvetica; display:inline-block; margin: 0px auto;
 text-align: center; background-color: #000000;}
 h1{color: #FFFF00; padding: 2vh;}p{font-size: 1.5rem; color: FFFF00;}
 .button{display: inline-block; background-color: #FFFF00; color: black; border: none;
 border-radius: 4px; color: black; text-decoration: none;
 font-size: 30px; width:100%}
 .button2{background-color: #FFFF00; color: black; width:49%}
 </style></head>
 <body> <h1>Team One Rover Web Server</h1>
 <p>Rover : <strong>BEAST MODE</strong></p>
 <p><a href='/?forward'><button class="button">FORWARD</button></a></p>
 <p><a href='/?left'><button class="button button2">LEFT</button></a>
 <a href='/?right'><button class="button button2" >RIGHT</button></a></p>
 <p><a href='/?backwards'><button class="button button">BACKWARDS</button></a></p>
 <p><a href='/?stop'><button class="button button">STOP</button></a></p>
 <p><a href='/?auto-pilot'><button class="button button2">AUTO-PILOT</button></a>
 <a href='/?manual'><button class="button button2">MANUAL</button></a></p>
 <p>Rover : <strong>SERVO CONTROLS</strong></p>
 <p><a href='/?forward'><button class="button">BASE SERVO</button></a></p>
 <p><a href='/?reset'><button class="button">RESET</button></a></p>
 <p><a href='/?180'><button class="button button2">180&#176;</button></a>
 <a href='/?160'><button class="button button2" >160&#176;</button></a></p>
 <p><a href='/?30'><button class="button button2">30&#176;</button></a>
 <a href='/?50'><button class="button button2" >50&#176;</button></a></p>
 <p><a href='/?100'><button class="button">100&#176;</button></a></p>
 <p><a href='/?forward'><button class="button">UP/DOWN SERVO</button></a></p>
 <a href='/?downupninty'><button class="button button2" >90&#176;</button></a></p>
 <p><a href='/?updownonetwenty'><button class="button button2">120&#176;</button></a>
 <a href='/?updownonefourty'><button class="button button2" >140&#176;</button></a></p>
 <p><a href='/?updownonesixty'><button class="button">160&#176;</button></a></p>
 <p><a href='/?forward'><button class="button">FORWARD/BACKWARD SERVO</button></a></p>
 <p><a href='/?100fb'><button class="button button2">100&#176;</button></a>
 <a href='/?120fb'><button class="button button2" >120&#176;</button></a></p>
 <p><a href='/?onesixtyfb'><button class="button button2">160&#176;</button></a>
 <a href='/?140fb'><button class="button button2" >140&#176;</button></a></p>
 <p><a href='/?forward'><button class="button">CLAW SERVO</button></a></p>
 <p><a href='/?open'><button class="button button2">OPEN</button></a>
 <a href='/?close'><button class="button button2" >CLOSE</button></a></p>
 </body></html>'''
    motor_state = "Stopped"
    if request.find('/?forward') > 0:
        motor_state="Going Forward"
        autoloop.Forward(400)
    if request.find('/?left') > 0:
        motor_state="Going Left"
        autoloop.Turn_Left(400)
    if request.find('/?right') > 0:
        motor_state="Going Right"
        autoloop.Turn_Right(400)
    if request.find('/?backwards') > 0:
        motor_state="Going Right"
        autoloop.Backwards(400)
    if request.find('/?stop') > 0:
        motor_state="Stopped"
        autoloop.Stop()

    return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('', 80))
s.listen(5)

while True:
    try:
        conn, addr = s.accept()
        print('Got a connection from %s' % str(addr))
        request = conn.recv(1024)
        request = str(request)
        print('Content = %s' % request)
        response = web_page(request)
        conn.send(response)
        conn.close()
    
        auto = request.find('/?auto-pilot')
        manual = request.find('/?manual')
        
        servo180 = request.find('/?180')
        servo160 = request.find('/?160')
        servo30 = request.find('/?30')
        servo50 = request.find('/?50')
        servo100 = request.find('/?100')
#         fiftydownup = request.find('/?fiftydownup')
        downupninty = request.find('/?downupninty')        
        updownonetwenty = request.find('/?updownonetwenty')
        updownonefourty = request.find('/updownonefourty')
        updownonesixty = request.find('/?updownonesixty')
        fb100 = request.find('/?100fb')
        fb120 = request.find('/?120fb')
        onesixtyfb = request.find('/?onesixtyfb')
        fb140 = request.find('/?140fb')
        servo_open = request.find('/?open')
        servo_close = request.find('/?close')
        servo_reset = request.find('/?reset')
          
        if servo180 > 0:
            servo_state="Going Forward"
            servo3.rotate(180)
        if servo160 > 0:
            servo_state="Going Left"
            servo3.rotate(160)
        if servo30 > 0:
            servo_state="Going Right"
            servo3.rotate(30)
        if servo50 > 0:
            servo_state="Stopped"
            servo3.rotate(50)
        if servo100 > 0:
            servo_state="Stopped"
            servo3.rotate(100)
        if downupninty > 0:
            servo_state="Going Left"
            servo2.rotate(90)
        if updownonetwenty > 0:
            servo_state="Going Right"
            servo2.rotate(120)
        if updownonefourty > 0:
            servo_state="Stopped"
            servo2.rotate(140)
        if updownonesixty > 0:
            servo_state="Stopped"
            servo2.rotate(160)
        if fb100 > 0:
            servo_state="Stopped"
            servo1.rotate(100)
        if fb120 > 0:
            servo_state="Stopped"
            servo1.rotate(120)
        if onesixtyfb > 0:
            servo_state="Stopped"
            servo1.rotate(160)
        if fb140 > 0:
            servo_state="Stopped"
            servo1.rotate(140)
        if servo_open > 0:
            servo_state="Stopped"
            servo4.rotate(110)
        if servo_close > 0:
            servo_state="Stopped"
            servo4.rotate(80)
        if servo_reset > 0:
            servo_state="Stopped"
            Reset()
        
        if auto > 0: #start autopilot
            print("autopilot on")
            globalVar.killThread = 0
            _thread.start_new_thread(autoloop.autopilot,())
               
        if manual > 0: #stop autopilot
            print("autopilot off")
            globalVar.killThread = 1
            autoloop.Stop()
            
        
    except KeyboardInterrupt:
        print("Ctrl + C pressed!")
        stop()
        sys.exit()