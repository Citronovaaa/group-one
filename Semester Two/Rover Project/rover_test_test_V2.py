import time
try:
    import usocket as socket
except:
    import socket

from machine import Pin
import network
from machine import PWM, Pin
from time import sleep
from hcsr04 import HCSR04
import machine
from time import sleep_ms

# Define the left and right motor pins. Change accordingly.
frequency = 15000
def LFW(speed, stbval, phaval):
    p =PWM(Pin(15), freq=frequency, duty=speed)
    stb = Pin(4, Pin.OUT)
    pha_b = Pin(0, Pin.OUT)

    stb.value(stbval)
    pha_b.value(phaval)

def RFW(speed, stbval, phaval):
    p = PWM(Pin(12), freq=frequency, duty=speed)
    stb = Pin(14, Pin.OUT)
    pha_a = Pin(13, Pin.OUT)

    stb.value(stbval)
    pha_a.value(phaval)

def LRW(speed, stbval, phaval):
    p = PWM(Pin(16), freq=frequency, duty=speed)
    stb = Pin(4, Pin.OUT)
    pha_a = Pin(17, Pin.OUT)

    stb.value(stbval)
    pha_a.value(phaval)

def RRW(speed, stbval, phaval):
    p = PWM(Pin(26), freq=frequency, duty=speed)
    stb = Pin(14, Pin.OUT)
    pha_b = Pin(27, Pin.OUT)

    stb.value(stbval)
    pha_b.value(phaval)

# Setup AP. Remember to change essid.
station = network.WLAN(network.AP_IF)
station.active(True)
station.config(essid = 'team_one')
station.config(authmode=3, password = '12345678')

while station.isconnected() == False:
    pass

print('Connection successful')
print(station.ifconfig())

def Forward(speed):
    LFW(speed,1,0)
    RFW(speed,1,1)
    LRW(speed,1,0)
    RRW(speed,1,1) 

def Backwards(speed):
    LFW(speed,1,1)
    RFW(speed,1,0)
    LRW(speed,1,1)
    RRW(speed,1,0) 

def Turn_Left(speed):
    LFW(0,1,1)
    RFW(speed,1,1)
    LRW(0,1,1)
    RRW(speed,1,1)

def Turn_Right(speed):
    LFW(speed,1,0)
    RFW(0,1,1)
    LRW(speed,1,0)
    RRW(0,1,1)

def Stop():
    LFW(0,0,0)
    RFW(0,0,0)
    LRW(0,0,0)    
    RRW(0,0,0)

def map(x, in_min, in_max, out_min, out_max):
    return int((x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min)
    
class GORILLACELL_SERVO:
    def __init__(self, signal_pin):
        self.pwm = PWM(Pin(signal_pin), freq=50, duty=75)

    def rotate(self, angle):
        self.pwm.duty(map(angle, 0, 180, 23, 124))

servo1 = GORILLACELL_SERVO(signal_pin=23)
servo2 = GORILLACELL_SERVO(signal_pin=22)
servo3 = GORILLACELL_SERVO(signal_pin=19)
servo4 = GORILLACELL_SERVO(signal_pin=21)

left_sensor = HCSR04(trigger_pin = 5, echo_pin = 18, echo_timeout_us = 10000)
right_sensor = HCSR04(trigger_pin = 25, echo_pin = 33, echo_timeout_us = 10000)

auto_pilot_on = False

def Auto_Pilot():
    global auto_pilot_on
    while auto_pilot_on == True:
        print("auto")
        Forward(600)
        left_distance = left_sensor.distance_cm()
        right_distance = right_sensor.distance_cm()
        servo1 = GORILLACELL_SERVO(signal_pin=23)
        servo2 = GORILLACELL_SERVO(signal_pin=22)
        servo3 = GORILLACELL_SERVO(signal_pin=19)
        servo4 = GORILLACELL_SERVO(signal_pin=21)
        if left_distance < 4:
            print(left_distance)
            Stop()
            sleep(2)
            Turn_Right(600)
            sleep(0.7)
            Turn_Left(600)
            sleep(0.5)
        if right_distance < 4:
            Stop()
            sleep(2)
            Turn_Left(600)
            sleep(0.7)
            Turn_Right(600)
            sleep(0.5)
            print("hello")
        
def Manual():
    global auto_pilot_on
    auto_pilot_on == False
    servo1 = GORILLACELL_SERVO(signal_pin=23)
    servo2 = GORILLACELL_SERVO(signal_pin=22)
    servo3 = GORILLACELL_SERVO(signal_pin=19)
    servo4 = GORILLACELL_SERVO(signal_pin=21)
    
    
# auto_pilot_on = False
# 
# def Auto_Pilot():
#     global auto_pilot_on
#     auto_pilot_on = True
#     
# def Manual():
#     global auto_pilot_on
#     auto_pilot_on = False
#     servo1 = GORILLACELL_SERVO(signal_pin=23)
#     servo2 = GORILLACELL_SERVO(signal_pin=22)
#     servo3 = GORILLACELL_SERVO(signal_pin=19)
#     servo4 = GORILLACELL_SERVO(signal_pin=21)

def web_page(request):
    html = '''<html><head> <title>Rover Web Server</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="icon" href="data:,"> <style>
 html{font-family: Helvetica; display:inline-block; margin: 0px auto;
 text-align: center; background-color: #000000;}
 h1{color: #FFFF00; padding: 2vh;}p{font-size: 1.5rem; color: FFFF00;}
 .button{display: inline-block; background-color: #FFFF00; color: black; border: none;
 border-radius: 4px; color: black; text-decoration: none;
 font-size: 30px; width:100%}
 .button2{background-color: #FFFF00; color: black; width:49%}
 </style></head>
 <body> <h1>Team One Rover Web Server</h1>
 <p>Rover : <strong>BEAST MODE</strong></p>
 <p><a href='/?forward'><button class="button">FORWARD</button></a></p>
 <p><a href='/?left'><button class="button button2">LEFT</button></a>
 <a href='/?right'><button class="button button2" >RIGHT</button></a></p>
 <p><a href='/?backwards'><button class="button button">BACKWARDS</button></a></p>
 <p><a href='/?stop'><button class="button button">STOP</button></a></p>
 <p><a href='/?auto-pilot'><button class="button button2">AUTO-PILOT</button></a>
 <a href='/?manual'><button class="button button2">MANUAL</button></a></p>
 <p>Rover : <strong>SERVO CONTROLS</strong></p>
 <p><a href='/?forward'><button class="button">BASE SERVO</button></a></p>
 <p><a href='/?180'><button class="button button2">180&#176;</button></a>
 <a href='/?160'><button class="button button2" >160&#176;</button></a></p>
 <p><a href='/?30'><button class="button button2">30&#176;</button></a>
 <a href='/?50'><button class="button button2" >50&#176;</button></a></p>
 <p><a href='/?100'><button class="button">100&#176;</button></a></p>
 <p><a href='/?forward'><button class="button">UP/DOWN SERVO</button></a></p>
 <p><a href='/?50downup'><button class="button button2">50&#176;</button></a>
 <a href='/?30updown'><button class="button button2" >30&#176;</button></a></p>
 <p><a href='/?20updown'><button class="button button2">20&#176;</button></a>
 <a href='/?10updown'><button class="button button2" >10&#176;</button></a></p>
 <p><a href='/?90updown'><button class="button">90&#176;</button></a></p>
 <p><a href='/?forward'><button class="button">FORWARD/BACKWARD SERVO</button></a></p>
 <p><a href='/?100fb'><button class="button button2">100&#176;</button></a>
 <a href='/?120fb'><button class="button button2" >120&#176;</button></a></p>
 <p><a href='/?160fb'><button class="button button2">160&#176;</button></a>
 <a href='/?140fb'><button class="button button2" >140&#176;</button></a></p>
 <p><a href='/?forward'><button class="button">CLAW SERVO</button></a></p>
 <p><a href='/?open'><button class="button button2">OPEN</button></a>
 <a href='/?close'><button class="button button2" >CLOSE</button></a></p>
 </body></html>'''
    motor_state = "Stopped"
    if request.find('/?forward') > 0:
        motor_state="Going Forward"
        Forward(600)
    if request.find('/?left') > 0:
        motor_state="Going Left"
        Turn_Left(600)
    if request.find('/?right') > 0:
        motor_state="Going Right"
        Turn_Right(600)
    if request.find('/?backwards') > 0:
        motor_state="Going Right"
        Backwards(600)
    if request.find('/?stop') > 0:
        motor_state="Stopped"
        Stop()
        servo4.rotate(80)
#         Manual()
    if request.find('/?auto-pilot') > 0:
        global autopilot_on
        auto_pilot_on = True
    if request.find('/?manual') > 0:
        Manual()
    
    servo_state = "Stopped"
    if request.find('/?180') > 0:
        servo_state="Going Forward"
        servo3.rotate(180)
    if request.find('/?160') > 0:
        servo_state="Going Left"
        servo3.rotate(160)
    if request.find('/?30') > 0:
        servo_state="Going Right"
        servo3.rotate(30)
    if request.find('/?50') > 0:
        servo_state="Stopped"
        servo3.rotate(50)
    if request.find('/?100') > 0:
        servo_state="Stopped"
        servo3.rotate(100)
    if request.find('/?50downup') > 0:
        servo_state="Going Forward"
        servo2.rotate(50)
    if request.find('/?30updown') > 0:
        servo_state="Going Left"
        servo2.rotate(30)
    if request.find('/?20updown') > 0:
        servo_state="Going Right"
        servo2.rotate(20)
    if request.find('/?10updown') > 0:
        servo_state="Stopped"
        servo2.rotate(10)
    if request.find('/?90updown') > 0:
        servo_state="Stopped"
        servo2.rotate(90)
    if request.find('/?100fb') > 0:
        servo_state="Stopped"
        servo1.rotate(100)
    if request.find('/?120fb') > 0:
        servo_state="Stopped"
        servo1.rotate(120)
    if request.find('/?160fb') > 0:
        servo_state="Stopped"
        servo1.rotate(160)
    if request.find('/?140fb') > 0:
        servo_state="Stopped"
        servo1.rotate(140)
    if request.find('/?open') > 0:
        servo_state="Stopped"
        servo4.rotate(110)
    if request.find('/?close') > 0:
        servo_state="Stopped"
        servo4.rotate(80)

    return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('', 80))
s.listen(5)

while True:
    conn, addr = s.accept()
    print('Got a connection from %s' % str(addr))
    request = conn.recv(1024)
    request = str(request)
    print('Content = %s' % request)
    response = web_page(request)
    conn.send(response)
    conn.close()
#     if auto_pilot_on == True:
# 
#         print("auto")
#         Forward(600)
#         left_distance = left_sensor.distance_cm()
#         right_distance = right_sensor.distance_cm()
#         servo1 = GORILLACELL_SERVO(signal_pin=23)
#         servo2 = GORILLACELL_SERVO(signal_pin=22)
#         servo3 = GORILLACELL_SERVO(signal_pin=19)
#         servo4 = GORILLACELL_SERVO(signal_pin=21)
#         if left_distance < 4:
#             print(left_distance)
#             Stop()
#             sleep(2)
#             Turn_Right(600)
#             sleep(0.7)
#             Turn_Left(600)
#             sleep(0.5)
#            continue
#         elif right_distance < 4:
#             Stop()
#             sleep(2)
#             Turn_Left(600)
#             sleep(0.7)
#             Turn_Right(600)
#             sleep(0.5)
#            continue
#        else: continue
