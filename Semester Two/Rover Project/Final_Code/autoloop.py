from machine import Pin, PWM
import machine
from time import sleep
from hcsr04 import HCSR04
import _thread
import globalVar

left_sensor = HCSR04(trigger_pin = 5, echo_pin = 18, echo_timeout_us = 10000)
right_sensor = HCSR04(trigger_pin = 25, echo_pin = 33, echo_timeout_us = 10000)

frequency = 800
def LFW(speed, stbval, phaval):
    p =PWM(Pin(15), freq=frequency, duty=speed)
    stb = Pin(4, Pin.OUT)
    pha_b = Pin(0, Pin.OUT)

    stb.value(stbval)
    pha_b.value(phaval)

def RFW(speed, stbval, phaval):
    p = PWM(Pin(12), freq=frequency, duty=speed)
    stb = Pin(14, Pin.OUT)
    pha_a = Pin(13, Pin.OUT)

    stb.value(stbval)
    pha_a.value(phaval)

def LRW(speed, stbval, phaval):
    p = PWM(Pin(16), freq=frequency, duty=speed)
    stb = Pin(4, Pin.OUT)
    pha_a = Pin(17, Pin.OUT)

    stb.value(stbval)
    pha_a.value(phaval)

def RRW(speed, stbval, phaval):
    p = PWM(Pin(26), freq=frequency, duty=speed)
    stb = Pin(14, Pin.OUT)
    pha_b = Pin(27, Pin.OUT)

    stb.value(stbval)
    pha_b.value(phaval)

def Forward(speed):
    LFW(speed,1,0)
    RFW(speed,1,1)
    LRW(speed,1,0)
    RRW(speed,1,1) 

def Backwards(speed):
    LFW(speed,1,1)
    RFW(speed,1,0)
    LRW(speed,1,1)
    RRW(speed,1,0) 

def Turn_Left(speed):
    LFW(0,1,1)
    RFW(speed,1,1)
    LRW(0,1,1)
    RRW(speed,1,1)

def Turn_Right(speed):
    LFW(speed,1,0)
    RFW(0,1,1)
    LRW(speed,1,0)
    RRW(0,1,1)

def Stop():
    LFW(0,0,0)
    RFW(0,0,0)
    LRW(0,0,0)    
    RRW(0,0,0)


def autopilot():
    while True:
        if globalVar.killThread == 1:
            _thread.exit()
            globalVar.killThread = 0
        Forward(400)
        left_distance = left_sensor.distance_cm()
        right_distance = right_sensor.distance_cm()
        if left_distance < 4:
            Stop()
            sleep(2)
            Turn_Right(600)
            sleep(0.7)
            Turn_Left(600)
            sleep(0.5)
            continue
        elif right_distance < 4:
            Stop()
            sleep(2)
            Turn_Left(600)
            sleep(0.7)
            Turn_Right(600)
            sleep(0.5)
            continue
        else: continue
