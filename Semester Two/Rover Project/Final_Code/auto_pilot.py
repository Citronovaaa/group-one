from hcsr04 import HCSR04
from machine import PWM, Pin
from time import sleep

left_sensor = HCSR04(trigger_pin = 5, echo_pin = 18, echo_timeout_us = 10000)
right_sensor = HCSR04(trigger_pin = 25, echo_pin = 33, echo_timeout_us = 10000)
auto_pilot_on = False

class GORILLACELL_SERVO:
    def __init__(self, signal_pin):
        self.pwm = PWM(Pin(signal_pin), freq=50, duty=75)

    def rotate(self, angle):
        self.pwm.duty(map(angle, 0, 180, 23, 124))

def Forward(speed):
    LFW(speed,1,0)
    RFW(speed,1,1)
    LRW(speed,1,0)
    RRW(speed,1,1) 

def Backwards(speed):
    LFW(speed,1,1)
    RFW(speed,1,0)
    LRW(speed,1,1)
    RRW(speed,1,0) 

def Turn_Left(speed):
    LFW(0,1,1)
    RFW(speed,1,1)
    LRW(0,1,1)
    RRW(speed,1,1)

def Turn_Right(speed):
    LFW(speed,1,0)
    RFW(0,1,1)
    LRW(speed,1,0)
    RRW(0,1,1)

def Stop():
    LFW(0,0,0)
    RFW(0,0,0)
    LRW(0,0,0)    
    RRW(0,0,0)
    
frequency = 15000
def LFW(speed, stbval, phaval):
    p =PWM(Pin(15), freq=frequency, duty=speed)
    stb = Pin(4, Pin.OUT)
    pha_b = Pin(0, Pin.OUT)

    stb.value(stbval)
    pha_b.value(phaval)

def RFW(speed, stbval, phaval):
    p = PWM(Pin(12), freq=frequency, duty=speed)
    stb = Pin(14, Pin.OUT)
    pha_a = Pin(13, Pin.OUT)

    stb.value(stbval)
    pha_a.value(phaval)

def LRW(speed, stbval, phaval):
    p = PWM(Pin(16), freq=frequency, duty=speed)
    stb = Pin(4, Pin.OUT)
    pha_a = Pin(17, Pin.OUT)

    stb.value(stbval)
    pha_a.value(phaval)

def RRW(speed, stbval, phaval):
    p = PWM(Pin(26), freq=frequency, duty=speed)
    stb = Pin(14, Pin.OUT)
    pha_b = Pin(27, Pin.OUT)

    stb.value(stbval)
    pha_b.value(phaval)

def Auto_Pilot():
    global auto_pilot_on
    auto_pilot_on = True

def Auto_Pilot_On():
    while True:
        Forward(400)
        left_distance = left_sensor.distance_cm()
        right_distance = right_sensor.distance_cm()
        servo1 = GORILLACELL_SERVO(signal_pin=23)
        servo2 = GORILLACELL_SERVO(signal_pin=22)
        servo3 = GORILLACELL_SERVO(signal_pin=19)
        servo4 = GORILLACELL_SERVO(signal_pin=21)
        if left_distance < 4:
            Stop()
            sleep(2)
            Turn_Right(600)
            sleep(0.7)
            Turn_Left(600)
            sleep(0.5)
            continue
        elif right_distance < 4:
            Stop()
            sleep(2)
            Turn_Left(600)
            sleep(0.7)
            Turn_Right(600)
            sleep(0.5)
            continue
        else: continue
