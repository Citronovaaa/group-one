from machine import PWM, Pin
from time import sleep
frequency = 800
def LFW(speed, stbval, phaval):
    p =PWM(Pin(15), freq=frequency, duty=speed)
    stb = Pin(4, Pin.OUT)
    pha_b = Pin(0, Pin.OUT)

    stb.value(stbval)
    pha_b.value(phaval)

def RFW(speed, stbval, phaval):
    p = PWM(Pin(12), freq=frequency, duty=speed)
    stb = Pin(14, Pin.OUT)
    pha_a = Pin(13, Pin.OUT)

    stb.value(stbval)
    pha_a.value(phaval)

def LRW(speed, stbval, phaval):
    p = PWM(Pin(16), freq=frequency, duty=speed)
    stb = Pin(4, Pin.OUT)
    pha_a = Pin(17, Pin.OUT)

    stb.value(stbval)
    pha_a.value(phaval)

def RRW(speed, stbval, phaval):
    p = PWM(Pin(26), freq=frequency, duty=speed)
    stb = Pin(14, Pin.OUT)
    pha_b = Pin(27, Pin.OUT)

    stb.value(stbval)
    pha_b.value(phaval)
"""
def Right_wheels(move,speed):
    if move == "f":
    RFW(speed,1,1)
    RRW(speed,1,1) 
    elif move == "b":
        RFW(speed,1,0)
        RRW(speed,1,0)
    else:
        RFW(0,0,0)
        RRW(0,0,0)

def Left_wheels(move, speed):
    if move == "f":
    LFW(speed,1,1)
    LRW(speed,1,1) 
    elif move == "b":
        LFW(speed,1,0)
        LRW(speed,1,0)
    else:
        LFW(0,0,0)
        LRW(0,0,0)
"""
def Forward(speed):
    LFW(speed,1,0)
    RFW(speed,1,1)
    LRW(speed,1,0)
    RRW(speed,1,1) 

def Backwards(speed):
    LFW(speed,1,1)
    RFW(speed,1,0)
    LRW(speed,1,1)
    RRW(speed,1,0) 

def Turn_Left(speed):
    LFW(0,1,1)
    RFW(speed,1,1)
    LRW(0,1,1)
    RRW(speed,1,1)

def Turn_Right(speed):
    LFW(speed,1,0)
    RFW(0,1,1)
    LRW(speed,1,0)
    RRW(0,1,1)

def Stop():
    LFW(0,0,0)
    RFW(0,0,0)
    LRW(0,0,0)    
    RRW(0,0,0)
