# Embedded Week 11 Questions


## Think of examples of smart city solutions. List at least 3 that you have seen implemented in Odense or other cities. If nothing comes to mind, research a bit to find out what some cities are doing.

I think the most impressive example I saw in the readings was the case of the smart poles being used to produce low energy cost light, pollution monitoring, as well as potential electric vehicle charging. If this could be scaled up for subburban neighborhoods, I think it could really be a great boost for the electrical vehical initiative. 

Waste management solutions could very much come in handy if they are not currently being utilized in Odense. For example, my apartment building produces a lot of waste (I'm not sure how/why) and this causes our trash can to be full more often than they are being emptied, so if there were a system that could alert waste management trucks to deploy and empty the full bins more often, that would be a perfect solution.  

## How do you see IoT devices play a role in these different scenarios? What protocols and sensors would they use?

In the first scenario, there would need to be some kind of air quality sensor, that would send the collected data to a database that could be analyzed. There would also need to be a sensor used for sensing light level, so they lights come on when it is dark, and dimm during the night when there is less pedestrian traffic. Mqtt would probably be used, or some sort of cloud data retrieval system. 

## How can you create systems that increase citizen welfare? How would you do it? Envision at least 2 examples of cases that you could see yourself solve.

I believe the most logical way to approach creating a system to increase citizen welfare is to first find a problem to which there is a viable soluiton. There are many sensors that can yield valuable data, and that data can be interpreted to develop systems that increase the welfare of citizens. 

I could see myself assisting in the smart pole example listed above. This could be life changing for environmental welfare if implimented on a wide scale. If street parking spaces were equiped with electric vehicle charging points, it would encourage more people to invest in electric vehicle purchases. This could be scaled up to parking lots with charging ports on a row of parking spaces, making the EV transition even more convenient. 


## Research metrics you want to track for a smart city indoor environment. Find sensors for each metric, and also alternatives for each. List the prices and link the datasheets.

- [ ] [Dust Sensor - GP2Y1010AU0F : Link to Datasheet](https://global.sharp/products/device/lineup/data/pdf/datasheet/gp2y1010au_e.pdf)
- [ ] [Gas Sensor - MQ-135 : Link to Datasheet](https://www.electronicoscaldas.com/datasheet/MQ-135_Hanwei.pdf)
- [ ] [Temperature and Humidity Sensor : Link to Datasheet](https://www.mouser.com/datasheet/2/783/BST-BME280-DS002-1509607.pdf)
- [ ] [PIR Sensor : Link to Datasheet](https://www.mpja.com/download/31227sc.pdf)
- [ ] We also have a camera that can be used on the rover. 