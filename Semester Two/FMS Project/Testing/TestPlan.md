# Test Plan for Product Testing

This plan contains information how the tester of the software will ensure that the product works as intended.

## Test Cases

TODO complete with more test cases

| Test Case Type | Description                                                         | Test Steps                                                                                                       | Expected Result                                                            | Status           |
| -------------- | ------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------- | ---------------- |
| Functionality  | UI should display Temperature, Humidity, Pressure Measurements      | 1. Measure data <br> 2. Go to Node-Red UI <br> 3. Are the measurements displayed as numbers (raw data)?          | The measurements are correctly displayed in the UI in their according unit | Passed or Failed |
| Functionality  | UI should display measurements as graphical representation (graphs) | 1. Measure data <br> 2. Go to Node Red UI <br> 3. Are the measurements displayed as graphs?                      | The measured data are correctly displayed in UI as graphs                  |
| Persistence    | Measurements shall be stored correctly in DB                        | Measure data, store to DB and look in DB if stored correctly                                                     | The measrurements are stored in the DB                                     |
| Connection     | MQTT...                                                             |
| Functionality  | UI reacts to new measurements and displays them                     | 1. Measure data <br> 2. Go to Node Red UI <br> 3. See the measurements/ data that changing as they are displayed | The values are changing in the UI                                          |

## Unit Testing

Unit tests in Python can be written to test the functionality of the code that was written. <br>
**Required Materials**: Python Unit Testing Framework
