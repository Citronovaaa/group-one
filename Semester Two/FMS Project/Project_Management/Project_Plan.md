# Project Plan

## Background

## Problem Statement

The data Center in its current state does not have a way of monitoring the air flow, temperature, humidity, or pressure on the cold and hot isles of the server room. This data is needed to optimize the uptime of the data center and performance of the servers.

## Purpose

The purpose of this project is to create a system that can provide the FMS students with vital data that will facilitate them optimizing the environment in the server room of their data center.

## Goals

The overall system that is going to be build looks as follows:

- Six BME280 sensors - to read temperature, humidity, and pressure
- One RPi Pico - to capture readings from the BME280 sensors
- One RPI - for communication to the database

The data that were measured will be stored in a database. It will be possible for students at FMS to access a web server where the measurements will be displayed. The data will be displayed in both forms, raw data as well as processed data in form of graphs.

## Project Scope

In Scope

- Retrieve temperature, pressure, humidity data (measurements)
- Store measurements in database
- Fetch data from database
- Process data from database and make them display in UI on web server

Out Scope

- Authentication client?
- security (i.e. access control to the data center)

## Project Deliveries

Project deliveries are:

- A system that will read data from sensors positioned at the top, middle and bottom of a server case. This data will be relayed to a database, where OMEs can use the data to optimize the data center.
- Frontend: raw data and graph visualizations
- Backend: sensor for reading data
- Web Server on which the frontend runs
-

## Project Milestones

The following milestones can be defined for the project:

1. Proof of Concept (PoC)
2. Minimal Viable Product (MVP)
3. Documentation

<br> During the POC, the feasability of the product idea is verified and proven by creating a prototype of the product. Any issues, e.g. technical issues that may arise during execution are demonstrated and assessed.

<br> During the MVP, the functionalities of the MVP that were defined are realized so that the first version of the product already provides value to the client.

<br> In the last stage of the project, documentation will be elaborated on Gitlab Pages. Also, the product will be finalized.

### Why are Milestones done in a project?

- **Def Milestone**: specific point in time in a project that symbolizes an achievement
- measure progress of the project toward its end goal
- mark important points for specific events, decisions or deliverables

### Work Breakdown Structure

This work breakdown structure is a deliverable diagram that shows an overview about all project's deliveries. <br>
[Work Breakdown Structure](https://gitlab.com/Citronovaaa/group-one/-/blob/main/FMS%20Project/Project_Management/Work_Breakdown_Structure.png)

## Schedule

According to the lecture plan, the deadlines are as follows:

- Week 15/16 Presentation hand in of POC
- Week 21 MVP presentations
- Week 22/23: Project Finalization, Writing Documentation on GitLab Pages
  The schedule is also showcased graphically in the Gantt Chart in the next sections.

### Gantt Chart

[Gantt Chart](https://gitlab.com/Citronovaaa/group-one/-/blob/main/FMS%20Project/Project_Management/Gantt_Chart.png)

## Organization

- [ ] Group One = Scrum Team
- [ ] FMS Project Manager: Kresten
- [ ] FMS Students

## Budget and resources

The budget is set by UCL and the resources needed at this state have been requested for purchase by UCL.

## Risk assessment

### Technical Risks

- _Problem_: The RPi drops connection to the database.
  _Solution_: The data will be stored locally on the Rpi.

- _Problem_: Physical components of the system fail (e.x. wires, sensors, etc.)
  _Solution_: Notification is given to the team, via the web server, and a technician will fix the issue.

- _Probem_: Lose power to the system.
  _Solution_: There will be backup power from the UPS.

- _Problem_: Someone without crediential can access the data/system.
  _Solution_: Security protocols and credientialed access.

- _Problem_: Cooling system shuts down.
  _Solution_: Notification for needing to hit kill switch.

### Natural Disaster Risks

- _Problem_: Natural Disasters - Total system shut down.

### Risks in Development

- _Problem_: Poorly defined requirements. _Solution_: Keep close contact to the project's stakeholders to make sure the requirements were written down correctly and that the customer receives a product in the end that matches his expectations and provides an actual value to him
- _Problem_: Bad team communication so that tasks are not clear to each member. _Solution_: Keep track of what needs to be done on an organizational board and assign team members to them

## Stakeholders

Internal: Group One

External: FMS/FMS students

## Communication

Communication between members of group one will take place primarily on Discord.

Communication with FMS will happen via the project manager Kresten's email.

## Success Criteria for the Project

The project will be defined as success if:

- the temperature, pressure and humidity of the data recks can be measured
- the temperature, pressure and humidity measurements are displayed on a web server that is accessible to the students at FMS to monitor the respective data
