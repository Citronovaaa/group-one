import time
from umqttsimple import MQTTClient
import ubinascii
import machine
import micropython
import network
import esp
from machine import Pin
from machine import Pin, SoftI2C #from the machine library
import BME280 as bme280 # the bme280 library
import time
esp.osdebug(None)
import gc
gc.collect()

ssid = 'HUAWEI P9'
password = '918f8f55c0ab'
mqtt_server = '192.168.43.91'
#EXAMPLE IP ADDRESS or DOMAIN NAME
#mqtt_server = '192.168.1.106'

client_id = ubinascii.hexlify(machine.unique_id())

topic_pub_temp = b'esp/bme/temperature'
topic_pub_press = b'esp/bme/pressure'
topic_pub_hum = b'esp/bme/humidity'
topic_pub_msg = b'Data_Center_Notification'

last_message = 0
message_interval = 5

station = network.WLAN(network.STA_IF)

station.active(True)
station.connect(ssid, password)

while station.isconnected() == False:
  pass

print('Connection successful')

i2c = SoftI2C(scl=Pin(22), sda=Pin(21), freq=10000)
sensor = bme280.BME280(i2c=i2c, address=0x77)

def connect_mqtt():
  global client_id, mqtt_server
  client = MQTTClient(client_id, mqtt_server)
  #client = MQTTClient(client_id, mqtt_server, user=your_username, password=your_password)
  client.connect()
  print('Connected to %s MQTT broker' % (mqtt_server))
  return client

def restart_and_reconnect():
  print('Failed to connect to MQTT broker. Reconnecting...')
  time.sleep(10)
  machine.reset()

def read_sensor():
  try:
    (temperature, pressure, humidity) = sensor.values
    return temperature, pressure, humidity
  #else:
    #return('Invalid sensor readings.')
  except OSError as e:
    return('Failed to read sensor.')

try:
  client = connect_mqtt()
except OSError as e:
  restart_and_reconnect()

while True:
  try:
    if (time.time() - last_message) > message_interval:
      temp, press, hum = read_sensor()
      message = "This is a notification that the temperature in Case One has exceeded the threshold. Temperature: " + temp
      print(temp)
      print(press)
      print(hum)
      client.publish(topic_pub_temp, temp)
      client.publish(topic_pub_press, press)
      client.publish(topic_pub_hum, hum)
      if temp > "24":
          client.publish(topic_pub_msg, message)
      last_message = time.time()
  except OSError as e:
    restart_and_reconnect()