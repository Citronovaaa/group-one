# Technical Requirements

This document specifies the technical requirements for the system. At the moment, the document summarizes only some ideas

## Components

The components and the relations between the components that are required can be seen in the deployment diagram that can be found [here]() (to be included).

## Protocols

The following protocols will be used for implementing the application:

- MQTT

## Coding language

The application will be built using the following languages:

- SQL: required for database management
- Python: Python will be used for programming the backend
- JSON

## Environment

The environment of the software is illustrated in the following deployment diagram: [here]() (to be included).

## Useful Links

- Link to information about MongoDB in NoedRed [https://flows.nodered.org/node/node-red-node-mongodb]
- Link to Youtube Video about NodeRed and databases [https://www.youtube.com/watch?v=d8eeNROMTv0]
- Link to information about UART connection form the Pico to the RPI [https://docs.micropython.org/en/latest/rp2/quickref.html#uart-serial-bus]
-
