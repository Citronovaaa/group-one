# User Stories 

## Template 
-	As a student at FMS, I want to be able to … to (reason why the feature is required)


## Stories 
### Students at FMS
-	As a student at FMS, I want to be able to access a web page where I can see all the temperature, pressure and humidity measurements from the data racks to monitor and ensure that every value is correct 
-	As a student at FMS, I want to be able to see the raw data from the temperature, pressure and humidity measurements so that I can evaluate if everything is working fine and correct
-	As a student at FMS, I want to be able to see data graphs that display the data from the temperature, pressure and humidity measurements so that I can have a good overview about those values
-	As a student at FMS, I want to be able to filter/ group the data on the webpage by some criteria to be able to find the exact values I am looking for
-	As a student at FMS, I want to be able to view data from a long time ago to be able to compare them to newer values to detect any noticeable problems/ salience 
-	As a student at FMS, I want to be able to retrieve as many data as possible to ensure that everything works as intended 
-	_As a student at FMS, I want to be notified in case that any value is not as expected so that I can figure out immediately what the problem is and then resolve the problem to keep the data save _
-	As a student at FMS, I want to be able to see the emergency status of the emergency notification so that I know exactly what went wrong and I can take action more quickly 
