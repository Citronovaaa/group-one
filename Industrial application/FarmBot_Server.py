from opcua import Server
import time

server = Server()
url = "opc.tcp://localhost:4841"
server.set_endpoint(url)

name = "OPCUA_SERVER"
addspace = server.register_namespace(name)

node = server.get_objects_node()
Param = node.add_object(addspace, "Parameters")

varX = Param.add_variable(addspace, "X_Variable", 0)
varX.set_writable()

varY = Param.add_variable(addspace, "Y_Variable", 0)
varY.set_writable()

server.start()
print("Server started at {}".format(url))


def movex(x):
    text = 'G00 X{} Y0 Z1 Q0'.format(x)
    print(text)

def movey(y):
    text = 'G00 X0 Y{} Z1 Q0'.format(y)
    print(text)

while True:
    x = varX.get_value()
    y = varY.get_value()
    print('Setting X value: {}'.format(x))
    print('Setting Y value: {}'.format(y))
    movex(x)
    movey(y)
    time.sleep(1)
