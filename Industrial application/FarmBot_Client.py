from opcua import Client
import time

url = "opc.tcp://localhost:4841"
client = Client(url)

client.connect()
print("Client Connected")

varX = client.get_node('ns=2;i=2')
varY = client.get_node('ns=2;i=3')

while True:
    msg = input('Enter 1 to change X or 2 to change Y: ')
    if msg == '1':
        varX.set_value((input("Enter X: ")))
    elif msg == '2':
        varY.set_value((input("Enter Y: ")))