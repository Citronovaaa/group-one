import time
import serial

def movex(x):
    text = 'G00 X{} Y0 Z1 Q0'.format(x)
    print(text)
    arduinoData.write(bytes('F22 P2 V1 Q0', 'utf-8'))
    arduinoData.write(bytes(text, 'utf-8'))

def movey(y):
    text = 'G00 X0 Y{} Z1 Q0'.format(y)
    print(text)
    arduinoData.write(bytes('F22 P2 V1 Q0', 'utf-8'))
    arduinoData.write(bytes(text, 'utf-8'))

arduinoData = serial.Serial(port='com3', baudrate=115200, timeout=.1)

while True:
    while (arduinoData.inWaiting()==0):
        pass
    arduinoData.write(bytes('F22 P2 V1 Q0', 'utf-8'))
    dataPacket = arduinoData.readline()
    dataPacket=str(dataPacket,'utf-8')
    print(dataPacket)
    time.sleep(1)
    data = input("Enter 1 to enter X, 2 to Y, nothing to continue")
    if data == '1':
        movex(str(input("Enter X: ")))
    elif data == '2':
        movey(str(input("Enter Y: ")))
    time.sleep(1)
