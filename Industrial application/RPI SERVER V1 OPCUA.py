from opcua import Server
import time
import serial

#arduinoData = serial.Serial(port='com3', baudrate=115200, timeout=.1) #Change serial port
arduinoData = serial.Serial(port='/dev/ttyACM0', baudrate=115200, bytesize=8)

check_list = list()  #Create empty check_list, its used to check if variable is the same as in previous run

def init_server():
    server = Server()
    url = "opc.tcp://192.168.1.176:4830"
    server.set_endpoint(url)
    name = "OPCUA_SIMULATION_SERVER"
    addspace = server.register_namespace(name)
    node = server.get_objects_node()
    Param = node.add_object(addspace, "Parameters")
    global command
    command = Param.add_variable(addspace, "Command", 0)
    command.set_writable()
    server.start()
    print("Server started at {}".format(url))

def init_arduino():        #This function will wait until farmbot is ready to receive commands
    while (arduinoData.inWaiting()==0):
        pass
    dataPacket = arduinoData.readline()
    dataPacket = str(dataPacket,'utf-8')
    print(dataPacket)
    send_data('F22 P2 V1 Q0')

def send_data(data_to_send):  #This function will send data using serial communication to arduino
    arduinoData.write(bytes(data_to_send + '\r\n', 'utf-8'))

init_server()

while True:
    command_new = command.get_value()
    print(command_new)
#Conditions below will check if command value is the same as previous, so we wont send the same command multiple times
    if command_new not in check_list:
        check_list.append(command_new)
        init_arduino()
        time.sleep(1)
        print("sending data")
        send_data(command_new)
        pass
    if command_new in check_list:
        check_list = [v for v in check_list if v == command_new] #Delete all elements in list except actual. This will stop flooding
    print(check_list)
    time.sleep(2)



