import time
import serial


arduinoData = serial.Serial(port='com3', baudrate=115200, timeout=.1)

while True:
    while (arduinoData.inWaiting()==0):
        pass
    dataPacket = arduinoData.readline()
    dataPacket=str(dataPacket,'utf-8')
    print(dataPacket)
    data = input("Enter F command: ")
    arduinoData.write(bytes('F22 P2 V1 Q0', 'utf-8'))
    arduinoData.write(bytes(data, 'utf-8'))
    time.sleep(1)
