from opcua import Client
import time
import serial

arduinoData = serial.Serial(port='com3', baudrate=115200, timeout=.1) #Change serial port
url = "opc.tcp://192.168.1.176:4849"    #Set URL (probably local IP and OPCUA port)

client = Client(url)
check_list = list()  #Create empty check_list, its used to check if variable is the same as in previous run

client.connect()
print("Client connected!")


def init():        #This function will wait until farmbot is ready to receive commands
    while (arduinoData.inWaiting()==0):
        pass
    for i in range(5):
        dataPacket = arduinoData.readline()
        dataPacket = str(dataPacket,'utf-8')
        print(dataPacket)
        arduinoData.write(bytes('F22 P2 V1 Q0', 'utf-8'))

def send_data(data_to_send):  #This function will send data using serial communication to arduino
    for i in range(15):
        arduinoData.write(bytes(data_to_send, 'utf-8'))
        time.sleep(0.05)

while True:
    com = client.get_node("ns=2;i=2")
    command = com.get_value()
    TIME = client.get_node("ns=2;i=3")
    TIME_value = TIME.get_value()
    print(command)
    print(TIME_value)

#Conditions below will check if command value is the same as previous, so we wont send the same command multiple times
    if command not in check_list:
        check_list.append(command)
        print("sending data")
        send_data(command)
        pass
    if command in check_list:
        check_list = [v for v in check_list if v == command] #Delete all elements in list except actual. This will stop flooding
    print(check_list)
    time.sleep(2)



