import tkinter as tk
import serial
import time

root= tk.Tk()

canvas1 = tk.Canvas(root, width=400, height=300, relief='raised')
canvas1.pack()

#Entry Boxes
entry1 = tk.Entry(root)
entry2 = tk.Entry(root)
entry3 = tk.Entry(root) 
canvas1.create_window(65, 30, window=entry1)
canvas1.create_window(65, 70, window=entry2)
canvas1.create_window(65, 110, window=entry3)

#Labels 
label1 = tk.Label(root, text='Type X change')
label1.config(font=('helvetica', 10))
label2 = tk.Label(root, text='Type Y change')
label2.config(font=('helvetica', 10))
label3 = tk.Label(root, text='Type Z change')
label3.config(font=('helvetica', 10))

canvas1.create_window(45, 10, window=label1)
canvas1.create_window(45, 50, window=label2)
canvas1.create_window(45, 90, window=label3)

#Functions to move
def movex():
    x = entry1.get()
    print("moving X")
    for i in range(10):
        arduinoData.write(bytes("'G00 X{} Y0 Z0 Q0'".format(x), 'utf-8'))
        time.sleep(1)
    label4 = tk.Label(root, text='X changed', font=('helvetica', 10))
    canvas1.create_window(270, 30, window=label4)
def movey():
    y = entry2.get()
    print("Moving Y")
    for i in range(10):
        arduinoData.write(bytes("'G00 X0 Y{} Z0 Q0'".format(y), 'utf-8'))
        time.sleep(1)
    label5 = tk.Label(root, text='Y changed', font=('helvetica', 10))
    canvas1.create_window(270, 70, window=label5)
def movez():
    z = entry3.get()
    print("Moving Z")
    for i in range(10):
        arduinoData.write(bytes("'G00 X0 Y0 Z{} Q0'".format(z), 'utf-8'))
        time.sleep(1)
    label6 = tk.Label(root, text='Z changed', font=('helvetica', 10))
    canvas1.create_window(270, 110, window=label6)

#Function to start farmbot
def init():
    while (arduinoData.inWaiting()==0): #if true, it will be repeated again and again
        pass
    for i in range(5):
        dataPacket = arduinoData.readline()
        dataPacket = str(dataPacket,'utf-8')
        print(dataPacket)
        arduinoData.write(bytes('F22 P2 V1 Q0', 'utf-8'))
        #time.sleep(1)

#Buttons to send
button1 = tk.Button(text='Send X', command=movex, bg='brown', fg='white', font=('helvetica', 9, 'bold'))
button2 = tk.Button(text='Send Y', command=movey, bg='brown', fg='white', font=('helvetica', 9, 'bold'))
button3 = tk.Button(text='Send Z', command=movez, bg='brown', fg='white', font=('helvetica', 9, 'bold'))
canvas1.create_window(180, 30, window=button1)
canvas1.create_window(180, 70, window=button2)
canvas1.create_window(180, 110, window=button3)


#Serial connect
arduinoData = serial.Serial(port='com3', baudrate=115200, timeout=.1)



init()

root.mainloop()
