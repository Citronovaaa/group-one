#Libraries
import tkinter as tk
from opcua import Server
import datetime

#Variables
global sleep_time
sleep_time = 0.05

#canvas settings
root= tk.Tk()
canvas1 = tk.Canvas(root, width=400, height=400, relief='raised')
canvas1.pack()


#Entry Boxes
entry1 = tk.Entry(root) #For X
entry2 = tk.Entry(root) #For Y
entry3 = tk.Entry(root) #For Z
entry4 = tk.Entry(root) #For Command

canvas1.create_window(65, 30, window=entry1)
canvas1.create_window(65, 70, window=entry2)
canvas1.create_window(65, 110, window=entry3)
canvas1.create_window(65, 150, window=entry4)


#Labels 
label1 = tk.Label(root, text='Type X change') 
label1.config(font=('helvetica', 10))

label2 = tk.Label(root, text='Type Y change')
label2.config(font=('helvetica', 10))

label3 = tk.Label(root, text='Type Z change')
label3.config(font=('helvetica', 10))

label10 = tk.Label(root, text='Type command')
label10.config(font=('helvetica', 10))

canvas1.create_window(45, 10, window=label1)  #Type X
canvas1.create_window(45, 50, window=label2) #Type Y
canvas1.create_window(45, 90, window=label3) #Type Z
canvas1.create_window(45, 130, window=label10) #Type command


#Functions to move
def enter_command():          
    data_to_send = entry4.get()
    print(data_to_send)
    send_data(data_to_send)

def movex():
    x = entry1.get()
    data_to_send = str("G00 X{} Q0".format(x))
    print(data_to_send)
    send_data(data_to_send)
    label4 = tk.Label(root, text='X changed', font=('helvetica', 10))
    canvas1.create_window(270, 30, window=label4)

def movey():
    y = entry2.get()
    data_to_send = str("G00 Y{} Q0".format(y))
    print(data_to_send)
    send_data(data_to_send)
    label5 = tk.Label(root, text='Y changed', font=('helvetica', 10))
    canvas1.create_window(270, 70, window=label5)

def movez():
    z = entry3.get()
    data_to_send = str("G00 Z{} Q0".format(z))
    print(data_to_send)
    send_data(data_to_send)
    label6 = tk.Label(root, text='Z changed', font=('helvetica', 10))
    canvas1.create_window(270, 110, window=label6)

def sethome():
    data_to_send = str("F84 X1 Y1 Z1")       #1 or 0
    print(data_to_send)
    send_data(data_to_send)

def findXhome():
    data_to_send = str("F11")     
    print(data_to_send)
    send_data(data_to_send)

def findYhome():
    data_to_send = str("F12")     
    print(data_to_send)
    send_data(data_to_send)

def findZhome():
    data_to_send = str("F13")     
    print(data_to_send)
    send_data(data_to_send)

#Function to start farmbot
def init():
    server = Server()
    url = "opc.tcp://192.168.1.176:4849"
    server.set_endpoint(url)
    name = "OPCUA_SIMULATION_SERVER"
    addspace = server.register_namespace(name)
    node = server.get_objects_node()
    Param = node.add_object(addspace, "Parameters")
    global Time
    global command
    command = Param.add_variable(addspace, "Command", 0)
    Time = Param.add_variable(addspace, "Time", 0)
    command.set_writable()
    Time.set_writable()
    server.start()
    print("Server started at {}".format(url))
 
#Function to send data to sever
def send_data(data_to_send):
    TIME = datetime.datetime.now()
    command.set_value(data_to_send)
    Time.set_value(TIME)


#Buttons to send
button1 = tk.Button(text='Send X', command=movex, bg='brown', fg='white', font=('helvetica', 9, 'bold'))
button2 = tk.Button(text='Send Y', command=movey, bg='brown', fg='white', font=('helvetica', 9, 'bold'))
button3 = tk.Button(text='Send Z', command=movez, bg='brown', fg='white', font=('helvetica', 9, 'bold'))
button4 = tk.Button(text='Send command', command=enter_command, bg='brown', fg='white', font=('helvetica', 9, 'bold'))

button5 = tk.Button(text='Sethome', command=sethome, bg='brown', fg='white', font=('helvetica', 9, 'bold')) #Set current positions to zero
button6 = tk.Button(text='Find X home', command=findXhome, bg='brown', fg='white', font=('helvetica', 9, 'bold'))  #Find X home
button7 = tk.Button(text='Find Y home', command=findYhome, bg='brown', fg='white', font=('helvetica', 9, 'bold')) #Find Y home
button8 = tk.Button(text='Find Z home', command=findZhome, bg='brown', fg='white', font=('helvetica', 9, 'bold')) #Find Z home

canvas1.create_window(180, 30, window=button1)
canvas1.create_window(180, 70, window=button2)
canvas1.create_window(180, 110, window=button3)
canvas1.create_window(180, 150, window=button4)

canvas1.create_window(180, 190, window=button5)
canvas1.create_window(180, 230, window=button6)
canvas1.create_window(180, 270, window=button7)
canvas1.create_window(180, 310, window=button8)


init()

root.mainloop()
