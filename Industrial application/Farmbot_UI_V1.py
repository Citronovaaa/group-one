import time
import serial
import tkinter

#CONNECT TO ARDUINO
arduinoData = serial.Serial(port='com3', baudrate=115200, timeout=.1)

#FUNCTIONS
def changex100():
    print("moving X 100")
    for i in range(10):
        arduinoData.write(bytes('G00 X100 Y00 Z0 Q0', 'utf-8'))
        time.sleep(1)

def changey100():
    print("moving Y 100")
    for i in range(10):
        arduinoData.write(bytes('G00 X0 Y100 Z0 Q0', 'utf-8'))
        time.sleep(1)

def changez100():
    print("moving Z 100")
    for i in range(10):
        arduinoData.write(bytes('G00 X0 Y0 Z100 Q0', 'utf-8'))
        time.sleep(1)
def changex_100():
    print("moving X -100")
    for i in range(10):
        arduinoData.write(bytes('G00 X-100 Y00 Z0 Q0', 'utf-8'))
        time.sleep(1)

def changey_100():
    print("moving Y -100")
    for i in range(10):
        #arduinoData.write(bytes('G00 X0 Y-100 Z0 Q0', 'utf-8'))
        #time.sleep(1)

def changez_100():
    print("moving Z -100")
    for i in range(10):
        arduinoData.write(bytes('G00 X0 Y0 Z-100 Q0', 'utf-8'))
        time.sleep(1)


def setpos():     
    for i in range(10):
        arduinoData.write(bytes('F84 X1 Y1 Z1', 'utf-8'))
        time.sleep(1)

def gohome():
    for i in range(10):
        arduinoData.write(bytes('G28', 'utf-8'))
        time.sleep(1)

def init():
    while (arduinoData.inWaiting()==0): #if true, it will be repeated again and again
        pass
    for i in range(5):
        dataPacket = arduinoData.readline()
        dataPacket = str(dataPacket,'utf-8')
        print(dataPacket)
        arduinoData.write(bytes('F22 P2 V1 Q0', 'utf-8'))
        #time.sleep(1)


#WINDOW and BOARD
window = tkinter.Tk()
window.title("Farmbot Controll Panel")


#BUTTONS
tkinter.Button(window, text="MoveX(100)", command=changex100,bd=10, bg="azure", fg="blue", width=15, height=1).grid(row=1, column=1)
tkinter.Button(window, text="MoveY(100)", command=changey100,bd=10, bg="azure",width=15, height=1).grid(row=2, column=1)
tkinter.Button(window, text="MoveZ(100)", command=changez100,bd=10, fg="purple",bg="azure", width=15, height=1).grid(row=3, column=1)
tkinter.Button(window, text="MoveX(-100)", command=changex_100,bd=10, bg="azure", fg="blue", width=15, height=1).grid(row=1, column=2)
tkinter.Button(window, text="MoveY(-100)", command=changey_100,bd=10, bg="azure",width=15, height=1).grid(row=2, column=2)
tkinter.Button(window, text="MoveZ(-100)", command=changez_100,bd=10, fg="purple",bg="azure", width=15, height=1).grid(row=3, column=2)

init()







