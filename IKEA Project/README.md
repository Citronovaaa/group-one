# Background

This is the semester ITT3 project where we will work with a project, initated by a company. This project plan will cover the project, and general components of the project layout.

The overall project case is described at: [Project Description](https://docs.google.com/presentation/d/1nv7E1_KZ0Cnl_AGHrtKrAZemLqXkhhXedazM6y9Ebbo/edit#slide=id.g13a50092405_0_6)

# Purpose

The main goal is to have a system that can monitor the WiFi signal strength in an environment. Specifically, this project aims to solve WiFi "cold spot". issues in the IKEA warehouse in Odense, Denmark. The monitor will provide the warehouse forklift drivers with a way to monitor the WiFi strength in their immediate area. 

# Goals

## The overall system that is going to be build looks as follows:  

![Project Block Diagram](./Documentation/Pictures/IKEA_Project_Block_Diagram.png)

Reading from the left to the right: 

* The battery is used a a power supply for the ESP32 microcontroller.
* The ESP32 microcontroller is used to measure the WiFi strength levels in the warehouse.
* The LEDs are used to signify to the user, the WiFi strength in correspondance green for good strength, yellow for okay strength, and red for poor to go strength. 
* The OLED display is used to show the WiFi signal strength as a percentage as well as a "progress bar". 

Project deliveries are:  

* A system reading RSSI levels and using those levels to display useful WiFi strength data to the user.   
* Docmentation of all parts of the solution  
* Regular meetings with project stakeholders  
* Part I and Part II Documentation

# Schedule

![Project Timeline](./Documentation/Pictures/IKEA_ProjectII_Timeline.png)


# Organization

* The project is supervised by our lecturers, Ilias and Allan. 
* The project stakeholder is Sheriff Dibba.
* The project team is made up of: Mathias, Matus, Ahmed and Timmie. 



# Budget and resources

Small monetary resources are expected. In terms of manpower, only the people in the project group are expected to contrbute.


# Risk assessment

![Risk Analysis](./Documentation/Pictures/IKEA_Project_Risk_Analysis.png)


# Stakeholders

• Internal Stakeholder(s): Groups 3 and Lecturers

• External Stakeholder(s): Sheriff Dibba


# Communication

Our group has mutually decided to communicate promarity through Discord for general communication. Project material will be shared in this repository along with written documentation beings shared in our team Google Drive. 

# Perspectives

Primarily this project will serve as a learning experience for the group and a glimpse into a real-life stakeholder scenario. 

This project will also serve as a template for other similar projects in future semesters.

# Evaluation

*TBD*

# References

*TBD*


* This project is managed using gitlab.com. The groups is at [Project Gitlab](https://gitlab.com/Citronovaaa/group-one/-/tree/main/IKEA%20Project)