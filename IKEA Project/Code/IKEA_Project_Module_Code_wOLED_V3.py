import network
import time
import ssd1306
from machine import Pin, SoftI2C
sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)

led1=Pin(32,Pin.OUT)
led2=Pin(33,Pin.OUT)
led3=Pin(25,Pin.OUT)

i2c = SoftI2C(scl=Pin(22), sda=Pin(21))

oled_width = 128
oled_height = 64
oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

authmodes = ['Open', 'WEP', 'WPA-PSK' 'WPA2-PSK4', 'WPA/WPA2-PSK']   
networks = []   

test_network_ssid = 'Mathias iPhone'    


def scan_networks():
    print('Scaning')
    for (ssid, bssid, channel, RSSI, authmode, hidden) in sta_if.scan():
        networks.append("{:s}".format(ssid))
        networks.append("{}".format(RSSI))
        #print("{:s}".format(ssid))
        #print("{}".format(RSSI))

def not_found():
    print('not found')
    led1.value(0)
    led2.value(0)
    led3.value(0)

def excellent_connection():
    led1.value(1)

def great_connection():
    led1.value(1)
    time.sleep(0.5)
    led1.value(0)
    time.sleep(0.5)
    led1.value(1)
    time.sleep(0.5)
    led1.value(0)
    time.sleep(0.5)

def ok_connection():
    led2.value(1)

def poor_connection():
    led2.value(1)
    time.sleep(0.5)
    led2.value(0)
    time.sleep(0.5)
    led2.value(1)
    time.sleep(0.5)
    led2.value(0)
    time.sleep(0.5)

def bad_connection():
    led3.value(1)

def no_conection():
    led3.value(1)
    time.sleep(0.5)
    led3.value(0)
    time.sleep(0.5)
    led3.value(1)
    time.sleep(0.5)
    led3.value(0)
    time.sleep(0.5)
    
def oled_reset():
    oled.fill(0)
    oled.show()



while True:
    scan_networks()
    if test_network_ssid in networks:
        i = networks.index(test_network_ssid)
        strength = int(networks[i+1])
        networks.clear()
        if strength >= -30:
            excellent_connection()
            led2.value(0)
            led3.value(0)
            oled_reset()
            oled.text('Excellent Conn..', 0, 20)
            oled.text('WiFi Signal:100%', 0, 30)
            oled.text('■■■■■', 0, 40)
            oled.show()
        elif strength < -30 and strength >= -40:
            great_connection()
            led2.value(0)
            led3.value(0)
            oled_reset()
            oled.text('Great Connection', 0, 20)
            oled.text('WiFi Signal:80%', 0, 30)
            oled.text('■■■■', 0, 40)
            oled.show()
        elif strength < -40 and strength >= -50:
            ok_connection()
            led1.value(0)
            led3.value(0)
            oled_reset()
            oled.text('Ok Connection', 0, 20)
            oled.text('Wi-Fi Signal:60%', 0, 30)
            oled.text('■■■', 0, 40)
            oled.show()
        elif strength < -50 and strength >= -60:
            poor_connection()
            led2.value(0)
            led3.value(0)
            oled_reset()
            oled.text('Poor Connection', 0, 20)
            oled.text('WiFi Signal:40%', 0, 30)
            oled.text('■■', 0, 40)
            oled.show()
        elif strength < -60 and strength >= -70:
            bad_connection()
            led1.value(0)
            led2.value(0)
            oled_reset()
            oled.text('Bad Connection', 0, 20)
            oled.text('Wi-Fi Signal:20%', 0, 30)
            oled.text('■', 0, 40)
            oled.show()   
        else:
            no_conection()
            oled_reset()
            oled.text('No Connection', 0, 20)
            oled.text('WiFi Signal:0%', 0, 30)
            oled.text('', 0, 40)
            oled.show()
        print(strength)
    else:
        not_found()
        continue


