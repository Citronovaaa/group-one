import network
import time
from machine import Pin
sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)

led1=Pin(32,Pin.OUT)
led2=Pin(33,Pin.OUT)
led3=Pin(25,Pin.OUT)
led4=Pin(26,Pin.OUT)
led5=Pin(27,Pin.OUT)
led6=Pin(14,Pin.OUT)

authmodes = ['Open', 'WEP', 'WPA-PSK' 'WPA2-PSK4', 'WPA/WPA2-PSK']   
networks = []   

test_network_ssid = 'Kviknet-CD80'    


def scan_networks():
    print('Scaning')
    for (ssid, bssid, channel, RSSI, authmode, hidden) in sta_if.scan():
        networks.append("{:s}".format(ssid))
        networks.append("{}".format(RSSI))
        #print("{:s}".format(ssid))
        #print("{}".format(RSSI))

def not_found():
    print('not found')
    led1.value(0)
    led2.value(0)
    led3.value(0)
    led4.value(0)
    led5.value(0)
    led6.value(0)

def six():
    led1.value(1)
    led2.value(1)
    led3.value(1)
    led4.value(1)
    led5.value(1)
    led6.value(1)

def five():
    led1.value(0)
    led2.value(1)
    led3.value(1)
    led4.value(1)
    led5.value(1)
    led6.value(1)

def four():
    led1.value(0)
    led2.value(0)
    led3.value(1)
    led4.value(1)
    led5.value(1)
    led6.value(1)

def three():
    led1.value(0)
    led2.value(0)
    led3.value(0)
    led4.value(1)
    led5.value(1)
    led6.value(1)

def two():
    led1.value(0)
    led2.value(0)
    led3.value(0)
    led4.value(0)
    led5.value(1)
    led6.value(1)

def one():
    led1.value(0)
    led2.value(0)
    led3.value(0)
    led4.value(0)
    led5.value(0)
    led6.value(1)


while True:
    scan_networks()
    if test_network_ssid in networks:
        i = networks.index(test_network_ssid)
        strength = int(networks[i+1])
        networks.clear()
        if strength >= -30:
            six()
        elif strength < -30 and strength >= -40:
            five()
        elif strength < -40 and strength >= -50:
            four()
        elif strength < -50 and strength >= -60:
            three()
        elif strength < -60 and strength >= -70:
            two()
        else:
            one()
        print(strength)
    else:
        not_found()
        continue
