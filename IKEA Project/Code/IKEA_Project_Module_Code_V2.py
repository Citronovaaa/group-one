#importing libraries
import network
import time
from machine import Pin

sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)

#set Pins
led1=Pin(32,Pin.OUT)
led2=Pin(33,Pin.OUT)
led3=Pin(25,Pin.OUT)

#set possible authentication modes
authmodes = ['Open', 'WEP', 'WPA-PSK' 'WPA2-PSK4', 'WPA/WPA2-PSK']

#create empty list. This list will be used to save ssid and rssi of the network
networks = []   

#set ssid of the investigated network
test_network_ssid = 'iPhone'    

#function that will scan and save all networks found into "networks" list
def scan_networks():
    print('Scaning')
    for (ssid, bssid, channel, RSSI, authmode, hidden) in sta_if.scan():
        networks.append("{:s}".format(ssid))
        networks.append("{}".format(RSSI))
        #print("{:s}".format(ssid))
        #print("{}".format(RSSI))

#functions that are executed under certain conditions
def not_found():
    print('not found')
    led1.value(0)
    led2.value(0)
    led3.value(0)

def excellent_connection():
    led1.value(1)

def great_connection():
    led1.value(1)
    time.sleep(0.5)
    led1.value(0)
    time.sleep(0.5)
    led1.value(1)
    time.sleep(0.5)
    led1.value(0)
    time.sleep(0.5)

def ok_connection():
    led2.value(1)

def poor_connection():
    led2.value(1)
    time.sleep(0.5)
    led2.value(0)
    time.sleep(0.5)
    led2.value(1)
    time.sleep(0.5)
    led2.value(0)
    time.sleep(0.5)

def bad_connection():
    led3.value(1)

def no_conection():
    led3.value(1)
    time.sleep(0.5)
    led3.value(0)
    time.sleep(0.5)
    led3.value(1)
    time.sleep(0.5)
    led3.value(0)
    time.sleep(0.5)



#never ending loop
while True:
    scan_networks() #execute function that will scan & store all ssid and rssi into list: networks
    if test_network_ssid in networks: #check if investigated network is in networks list
        i = networks.index(test_network_ssid) #if true, find its index
        strength = int(networks[i+1]) #after index, there is always rssi of network. store it
        networks.clear() #clear networks list, since we wont need it anymore
        #now, execute functions that will light up/turn off LEDs based on the connection
        if strength >= -30: 
            excellent_connection()
            led2.value(0)
            led3.value(0)
        elif strength < -30 and strength >= -40:
            great_connection()
        elif strength < -40 and strength >= -50:
            ok_connection()
            led1.value(0)
            led3.value(0)
        elif strength < -50 and strength >= -60:
            poor_connection()
        elif strength < -60 and strength >= -70:
            bad_connection()
            led1.value(0)
            led2.value(0)
        else:
            no_conection()
        print(strength)
    #if investigated network wasnt found continue
    else:
        not_found()
        continue



