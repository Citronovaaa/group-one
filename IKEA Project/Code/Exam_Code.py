import network
import time
from machine import Pin

sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)

led1=Pin(19,Pin.OUT)
led2=Pin(18,Pin.OUT)
led3=Pin(16,Pin.OUT)

authmodes = ['Open', 'WEP', 'WPA-PSK' 'WPA2-PSK4', 'WPA/WPA2-PSK']   
networks = []   

test_network_ssid = 'iPhone'    


def scan_networks():
    print('Scaning')
    for (ssid, bssid, channel, RSSI, authmode, hidden) in sta_if.scan():
        networks.append("{:s}".format(ssid))
        networks.append("{}".format(RSSI))
        #print("{:s}".format(ssid))
        #print("{}".format(RSSI))

def not_found():
    print('not found')
    led1.value(0)
    led2.value(0)
    led3.value(0)

def excellent_connection():
    led1.value(1)

def great_connection():
    led1.value(1)
    time.sleep(0.5)
    led1.value(0)
    time.sleep(0.5)
    led1.value(1)
    time.sleep(0.5)
    led1.value(0)
    time.sleep(0.5)

def ok_connection():
    led2.value(1)

def poor_connection():
    led2.value(1)
    time.sleep(0.5)
    led2.value(0)
    time.sleep(0.5)
    led2.value(1)
    time.sleep(0.5)
    led2.value(0)
    time.sleep(0.5)

def bad_connection():
    led3.value(1)

def no_conection():
    led3.value(1)
    time.sleep(0.5)
    led3.value(0)
    time.sleep(0.5)
    led3.value(1)
    time.sleep(0.5)
    led3.value(0)
    time.sleep(0.5)
    
def oled_reset():
    oled.fill(0)
    oled.show()



while True:
    scan_networks()
    if test_network_ssid in networks:
        i = networks.index(test_network_ssid)
        strength = int(networks[i+1])
        networks.clear()
        if strength >= -30:
            excellent_connection()
            led2.value(0)
            led3.value(0)
        elif strength < -30 and strength >= -40:
            great_connection()
            led2.value(0)
            led3.value(0)
        elif strength < -40 and strength >= -50:
            ok_connection()
            led1.value(0)
            led3.value(0)
        elif strength < -50 and strength >= -60:
            poor_connection()
            led2.value(0)
            led3.value(0)
        elif strength < -60 and strength >= -70:
            bad_connection()
            led1.value(0)
            led2.value(0)
        else:
            no_conection()
        print(strength)
    else:
        not_found()
        continue


