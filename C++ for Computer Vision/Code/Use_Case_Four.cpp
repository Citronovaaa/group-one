#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include "detectors.h"


using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
	VideoCapture video("C:/opencv_images/test.mp4"); 

	if (!video.isOpened()) return -1;

	Mat frame;

	int frameWidth = video.get(CAP_PROP_FRAME_WIDTH);
	int frameHeight = video.get(CAP_PROP_FRAME_HEIGHT);

	VideoWriter output("output.avi", VideoWriter::fourcc('M', 'J', 'P', 'G'), 30, Size(frameWidth, frameHeight));

	
	

	while (video.read(frame))
	{
		frame = contours_full(frame);
		frame = Blob_detector(frame);

		output.write(frame);
		
		if (waitKey(25) >= 0)
		{
			break;
		}
	}

	output.release();
	video.release();

	destroyAllWindows;

	return 0;
}