#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include "chains.h"

using namespace cv;
using namespace std;

int main(int argc, char** argv) {
	Mat eroded;
	Mat image;

	image = imread("C:/opencv_images/man_bird.jpg");

	eroded = preprocessing(image);

	imshow("Before", image);
	imshow("After", eroded);
	imwrite("C:/opencv_images/preprocessing_noise.jpg", eroded);
	waitKey(0);
}