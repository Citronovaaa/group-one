#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <iostream>
#include "chains.h"

using namespace cv;
using namespace std;

int main(int argc, char** argv) {
	Mat imgBlur;
	Mat image;

	image = imread("C:/opencv_images/moose.jpg");

	GaussianBlur(image, imgBlur, Size(5, 5), 3, 0);

	imshow("Before", image);
	imshow("After", imgBlur);
	imwrite("C:/opencv_images/moose_toomuch_filter.jpg", imgBlur);
	waitKey(0);
}